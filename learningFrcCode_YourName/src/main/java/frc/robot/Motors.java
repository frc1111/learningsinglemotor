package frc.robot;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;

public class Motors {
    public static CANSparkMax testMotorDrive  = new CANSparkMax(14,  CANSparkMaxLowLevel.MotorType.kBrushless);
    public static CANSparkMax testMotorRotate  = new CANSparkMax(16,  CANSparkMaxLowLevel.MotorType.kBrushless);
}
