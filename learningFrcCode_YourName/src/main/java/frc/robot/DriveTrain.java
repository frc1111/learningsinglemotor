package frc.robot;

import frc.robot.Motors;

public class DriveTrain {

    public DriveTrain() {
    }

    public void driveTwoMotors(double motor1Power, double motor2Power) {
        double scaledPower = 0.1; // run motors at 10% power
        Motors.testMotorDrive.set(motor1Power * scaledPower);
        Motors.testMotorRotate.set(motor2Power * scaledPower);
    }
}
